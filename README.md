This project has been develop following this [yt video tutorial](https://youtu.be/7glCsF9fv3s)

Special thanks to CodeMonkey to teach how Unity Graphic engine works.

Project was maded using Unity Editor version **2021.3.18f1**. If checked to different version, there are problems about the read of project. Be sure to open the project in the Unity version specificed.

Project are splitted in parts due to limitations of upload file. To decompress, use a software like [7Zip](https://www.7-zip.org/download.html) or similar.
